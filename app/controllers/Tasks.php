<?php namespace TODO\controllers;

/**
 * Class Tasks
 * @package TODO\controllers
 *
 * tasks controller responsible for operation on tasks like editing adding and mark as done
 * And this controller is responsible for fetching from db by date or project id
 *
 */

class Tasks extends Controller {

    public function home() {

        $tasks = new \TODO\models\Tasks();
        $tasks = $tasks->byDate(1);

        switch ($_SESSION['sort']['method']) {
            case (1):
                $this->sort1 = $_SESSION['sort']['id'];
                break;
            case (2):
                $this->sort2 = $_SESSION['sort']['id'];
                break;
            default;
        }
        $messages['tasks']='';
        if (!empty($_SESSION['message_tasks'])) $messages['tasks'] = $_SESSION['message_tasks'];
        $_SESSION['message_tasks'] = '';

        echo $this->twig->render('task/home.html',array_merge([
            'title' => 'Home',
            'tasks' => $tasks,
            'sort1' => $this->sort1,
            'sort2' => $this->sort2,
            'messages' => $messages
        ], $this->array));
    }

    public function byProject($params = []) {

        $tasks = new \TODO\models\Tasks();
        $tasks = $tasks->byProject($params[0]);

        switch ($_SESSION['sort']['method']) {
            case (1):
                $this->sort1 = $_SESSION['sort']['id'];
                break;
            case (2):
                $this->sort2 = $_SESSION['sort']['id'];
                break;
            default;
        }

        $messages['tasks']='';
        if (!empty($_SESSION['message_tasks'])) $messages['tasks'] = $_SESSION['message_tasks'];
        $_SESSION['message_tasks'] = '';

        echo $this->twig->render('task/home.html',array_merge([
            'title' => 'Home',
            'tasks' => $tasks,
            'sort1' => $this->sort1,
            'sort2' => $this->sort2,
            'messages' => $messages
        ], $this->array));
    }

    public function byDate($params = []) {

        $tasks = new \TODO\models\Tasks();
        $tasks = $tasks->byDate($params[0]);

        switch ($_SESSION['sort']['method']) {
            case (1):
                $this->sort1 = $_SESSION['sort']['id'];
                break;
            case (2):
                $this->sort2 = $_SESSION['sort']['id'];
                break;
            default;
        }

        $messages['tasks']='';
        if (!empty($_SESSION['message_tasks'])) $messages['tasks'] = $_SESSION['message_tasks'];
        $_SESSION['message_tasks'] = '';

        echo $this->twig->render('task/home.html',array_merge([
            'title' => 'Home',
            'tasks' => $tasks,
            'sort1' => $this->sort1,
            'sort2' => $this->sort2,
            'messages' => $messages
        ], $this->array));
    }

    public function done($params = []) {
        $edit = new \TODO\models\Tasks();
        $edit->done($params[0]);

        header('Location:' . $this->location);
    }

    public function undone($params = []) {
        $edit = new \TODO\models\Tasks();
        $edit->undone($params[0]);

        header('Location:' . $this->location);
    }

    public function prepare($params = []) {

        $edit = new \TODO\models\Tasks();
        $edit->prepare($params[0]);

        $messages['tasks']='';
        if (!empty($_SESSION['message_tasks'])) $messages['tasks'] .= $_SESSION['message_tasks'];
        $_SESSION['message_tasks'] = '';

        echo $this->twig->render('task/confirm.html', array_merge([
            'title' => 'Edit task',
            'messages' => $messages,
            'idNumber' => $params[0]
        ], $this->array));

    }

    public function delete($params = []) {
        if ($params[0] !== 'undo') {

            $edit = new \TODO\models\Tasks();
            $result = $edit->prepare($params[0]);

            if ($result) {
                $edit->delete($params[0]);
            }
        }
        $_SESSION['message_tasks'] = '';
        header('Location:' . $this->location);
    }

    public function add($params = []) {
        $method = new \TODO\models\Tasks();
        $method = $method->add($params);

        if ($method){
            header('Location:' . $this->location);
            exit();
        };

        $id = 0;
        if ($_SESSION['sort']['method'] == 2){
            $id = $_SESSION['sort']['id'];
        }

        $messages['tasks']='';
        if (!empty($_SESSION['message_tasks'])) $messages['tasks'] = $_SESSION['message_tasks'];
        $_SESSION['message_tasks'] = '';

        echo $this->twig->render('task/add.html',array_merge( [
            'title' => 'Add task',
            'messages' => $messages,
            'id' => $id
        ], $this->array));
    }

    public function edit($params = []) {

        if (!isset($params[0])){
            header('Location:' . $this->location);
            exit();
        };

        if (isset($params['POST'])) {

            $tasks = new \TODO\models\Tasks();
            $tasks = $tasks->edit($params);

            if ($tasks){
                header('Location:' . $this->location);
                exit();
            };
        }
        $tasks = new \TODO\models\Tasks();
        $tasks = $tasks->one($params[0]);

        $messages['tasks'] = '';
        if (!empty($_SESSION['message_tasks'])) $messages['tasks'] = $_SESSION['message_tasks'];
        $_SESSION['message_tasks'] = '';

        echo $this->twig->render('task/edit.html', array_merge([
            'title' => 'Edit task',
            'tasks' => $tasks,
            'messages' => $messages
        ], $this->array));
    }
}