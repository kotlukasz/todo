<?php namespace TODO\controllers;

/**
 *
 * Project controller responsible for projects adding, editing, deleting.
 * prepare method responsible for checking if delete method is possible and giv info if in projects are tasks or other projects
 *
 */

class Projects extends Controller {

    public function home($params = []) {

        $editProjects = new \TODO\models\Projects();
        if (isset($params[0])) {
            $editProjects = $editProjects->one($params[0]);
        } else {
            $editProjects = $editProjects->one(0);
        }

        $messages['tasks']='';
        if (!empty($_SESSION['message_tasks'])) $messages['tasks'] = $_SESSION['message_tasks'];
        $_SESSION['message_tasks'] = '';

        echo $this->twig->render('project/edit.html',array_merge([
            'title' => 'Projects',
            'messages' => $messages,
            'editprojects' => $editProjects
        ], $this->array));
    }

    public function add($params = []) {

        if (isset($params['POST'])) {
            $editProjects = new \TODO\models\Projects();
            $editProjects = $editProjects->add($params);
            if ($editProjects){
                header('Location:' . $this->location);
                exit();
            };
        }

        $id = 0;
        if ($_SESSION['sort']['method'] == 2){
            $id = $_SESSION['sort']['id'];
        }

        $messages['tasks']= $_SESSION['message_tasks'];
        $_SESSION['message_tasks'] = '';

        echo $this->twig->render('project/add.html', array_merge([
            'title' => 'Add project',
            'messages' => $messages,
            'idNumber' => $id
        ], $this->array));
    }

    public function edit($params = []) {

        if (!isset($params[0])){
            header('Location:' . $this->location);
            exit();
        };

        if (isset($params['POST'])) {

            $editProjects = new \TODO\models\Projects();
            $editProjects = $editProjects->edit($params);

            if ($editProjects){
                header('Location:' . $this->setLocation(2, $params[0]));
                exit();
            };
        }
        $editProjects = new \TODO\models\Projects();
        $editProjects = $editProjects->one($params[0]);

        $messages['tasks'] = '';
        if (!empty($_SESSION['message_tasks'])) $messages['tasks'] = $_SESSION['message_tasks'];
        $_SESSION['message_tasks'] = '';

        echo $this->twig->render('project/edit.html', array_merge([
            'title' => 'Edit project',
            'messages' => $messages,
            'editprojects' => $editProjects
        ], $this->array));
    }

    public function prepare($params = []) {

        $edit = new \TODO\models\Projects();
        $edit->prepare($params[0]);

        $messages['tasks'] = '';
        if (!empty($_SESSION['message_tasks'])) $messages['tasks'] = $_SESSION['message_tasks'];
        $_SESSION['message_tasks'] = '';

        echo $this->twig->render('project/confirm.html', array_merge([
            'title' => 'Edit project',
            'messages' => $messages,
            'idNumber' => $params[0]
        ], $this->array));

    }

    public function delete($params = []) {
        if ($params[0] !== 'undo') {
            $edit = new \TODO\models\Projects();
            $edit->delete($params[0]);
        } else {
            header('Location:' . $this->location);
        }

        header('Location:' . $this->setLocation());
    }
}