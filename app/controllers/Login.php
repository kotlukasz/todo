<?php namespace TODO\controllers;

/**
 * Class Login
 * @package TODO\controllers
 *
 * Controller responsible for log on, log out and registry users.
 * Home method check if logged if yes redirect to app if not show forms
 * Other methods sends data to DB via models
 *
 */

class Login extends Controller {

    public function home() {
        $config = $this->config->setConfig(['paths','defaults']);
        $url = '';

        if ($_SESSION['userId'] !== NULL) {
            if (!empty($_SESSION['oldUrl'])) {
                if (mb_substr(strtolower($_SESSION['oldUrl']), 0, 5) != 'login') {
                    $url = $_SESSION['oldUrl'];
                }
                $_SESSION['oldUrl'] = '';
            }
            header('Location:' . $config['baseURL'] . $url);
            die();
        } else {
            $messages['login'] = '';
            $messages['registry'] = '';
            if (!empty($_SESSION['message_login'])) $messages['login'] = $_SESSION['message_login'];
            if (!empty($_SESSION['message_registry'])) $messages['registry'] = $_SESSION['message_registry'];
            $_SESSION['message_login'] = '';
            $_SESSION['message_registry'] = '';

            echo $this->twig->render('login/home.html',array_merge( [
                'title' => 'Login',
                'messages' => $messages
            ], $this->array));
        }
    }

    public function login($params = []) {
        $config = $this->config->setConfig(['defaults']);
        $method = new \TODO\models\Login();
        $method = $method->Login($params);

        if ($method) {
            header('Location:' . $config['baseURL']);
            die();
        } else {
            header('Location:' . $config['baseURL'].'login');
            die();
        }
    }

    public function registry($params = []) {
        $config = $this->config->setConfig(['defaults']);
        $method = new \TODO\models\Login();
        $method = $method->Registry($params);

        if ($method) {
            header('Location:' . $config['baseURL']);
            die();
        } else {
            header('Location:' . $config['baseURL'].'login');
            die();
        }
    }

    public function logout() {
        session_destroy();
        $config = $this->config->setConfig(['defaults']);
        header('Location:' . $config['baseURL']);
        die();
    }
}