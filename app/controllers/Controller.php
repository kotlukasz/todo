<?php namespace TODO\controllers;

/**
 * Class Controller
 * @package TODO\controllers
 *
 * Default controoler load all static data
 *
 */

class Controller {

    protected $config;
    protected $twig;
    protected $location;
    protected $menus;
    protected $icons;
    protected $projects;
    protected $array;
    protected $sort1;
    protected $sort2;

    public function __construct() {
        $this->config = new \TODO\main\Config();
        $config = $this->config->setConfig(['paths', 'defaults']);

        $this->location = $config['baseURL'];
        switch ($_SESSION['sort']['method']) {
            case (1):
                $this->location .= 'tasks/byDate/';
                if ($_SESSION['sort']['id'] !== NULL) {
                    $this->location .= $_SESSION['sort']['id'];
                } else {
                    $this->location .= '1';
                }
                break;
            case (2):
                if ($_SESSION['sort']['id'] !== NULL) {
                    $this->location .= 'tasks/byProject/'.$_SESSION['sort']['id'];
                } else {
                    $this->location .= 'tasks/byDate/1';
                }

                break;
            default;
        }

        $this->menus = new \TODO\models\Menus();
        $this->menus = $this->menus->all();

        $this->icons = new \TODO\models\Icons();
        $this->icons = $this->icons->all();

        $this->projects = new \TODO\models\Projects();
        $this->projects = $this->projects->all();

        $this->sort1 = '';
        $this->sort2 = '';

        $loader = new \Twig_Loader_Filesystem($config['views_path']);
        $this->twig = new \Twig_Environment($loader);

        $this->array = array(
            'configs' => $config,
            'menus' => $this->menus,
            'icons' => $this->icons,
            'projects' => $this->projects
        );
    }

    /**
     * @param int $method
     * @param int $id
     * @return string
     *
     * Change default location
     *
     */

    public function setLocation($method = 1, $id = 1) {
        $config = $this->config->setConfig(['paths', 'defaults']);
        $location = $config['baseURL'];

        switch ($method) {
            case (1):
                return $location .= 'tasks/byDate/'.$id;
                break;
            case (2):
                return $location .= 'tasks/byProject/'.$id;
                break;
            default;
        }
    }

    public function home() {
        header('Location:' . $this->config['baseURL']);
    }
}