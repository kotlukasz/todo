<?php namespace TODO\models;

class Projects {

    public function all() {
        $db = new \TODO\main\Db();
        $sql ='SELECT `id`, `name`, `parentId`, `iconId` FROM `projects` WHERE `userId` = :userId AND `deleted` = 0';
        $prep = $db->getInstance()->prepare($sql);
        $prep->bindValue(':userId', $_SESSION['userId'], \PDO::PARAM_INT);
        $prep->execute();
        return $results = $prep->fetchAll();
    }

    public function one($params) {
        $db = new \TODO\main\Db();
        $sql ='SELECT `id`, `name`, `parentId`, `iconId` FROM `projects` WHERE `userId` = :userId AND `id` = :id AND `deleted` = 0';
        $prep = $db->getInstance()->prepare($sql);
        $prep->bindValue(':userId', $_SESSION['userId'], \PDO::PARAM_INT);
        $prep->bindValue(':id', intval($params), \PDO::PARAM_INT);
        $prep->execute();
        $results = $prep->fetch();

        if (!empty($results)){
            return $results;
        } else {
            $_SESSION['message_tasks'] = 'There is no such task or You do not have permission to see this task.';
            return false;
        }
    }

    public function edit($params = []) {

        if (empty($params['POST'])) {
            $_SESSION['message_tasks'] = 'INVALID DATA';
            return false;
        } else {

            $all = $this->all();
            $all = array_combine(range(1, count($all)), array_values($all));

            /**
             *
             * check if project has child or grandchild and save it for validation down below
             * for information if it can move and where prevent to main project has only 2 levels of subprojects
             *
             */

            $child = [];

            foreach ($all as $project) {
                if ($project['parentId'] == intval($params[0])){
                    foreach ($all as $project2) {
                        if ($project['id'] == $project2['parentId']){
                            $child[2]['id'] = intval($params[0]);
                        } else {
                            $child[1]['id'] = intval($params[0]);
                        }
                    }
                }
            }

            /**
             *
             * check if project has parent or grandparent and save it for validation down below
             * for information if it can move and where prevent to main project has only 2 levels of subprojects
             *
             */

            foreach ($all as $project) {
                if (($project['id'] == intval($params['POST']['parent'])) && $project['parentId'] !=0){
                    foreach ($all as $project2) {
                        if ($project2['id'] == $project['parentId']){
                            foreach ($all as $project3) {
                                if ($project3['id'] == $project2['parentId']){
                                    $child[2]['parent'] = intval($params['POST']['parent']);
                                } else {
                                    $child[1]['parent'] = intval($params['POST']['parent']);
                                }
                            }
                        }
                    }
                }
            }

            $flag = true;
            if (((isset($child[2]['parent'])) || isset($child[2]['id'])) || ((isset($child[1]['id'])) && isset($child[1]['parent']))){
                $flag = false;
            }

            $db = new \TODO\main\Db();
            $sql ='SELECT `id`, `iconId` FROM `projects` WHERE ';

            if ($params['POST']['parent'] != 0) {
             $sql .= '`id` IN (:projectId, :parentId) AND `userId` = :userId AND `deleted` = 0 ORDER BY FIELD (`id`, :projectId2, :parentId2)';
            } else {
                $sql .= '`id` = :projectId AND `userId` = :userId AND `deleted` = 0';
            }

            $prep = $db->getInstance()->prepare($sql);
            $prep->bindValue(':projectId', $params[0], \PDO::PARAM_INT);

            if ($params['POST']['parent'] != 0) {
                $prep->bindValue(':projectId2', $params[0], \PDO::PARAM_INT);
                $prep->bindValue(':parentId', $params['POST']['parent'], \PDO::PARAM_INT);
                $prep->bindValue(':parentId2', $params['POST']['parent'], \PDO::PARAM_INT);
            }

            $prep->bindValue(':userId', $_SESSION['userId'], \PDO::PARAM_INT);
            $prep->execute();
            $results = $prep->fetchAll();

            if (($params['POST']['parent'] != 0 && count($results) == 2 ) || ( $params['POST']['parent'] == 0 && count($results) == 1)) {
                
                $validProjectName = trim($params['POST']['projectname']) && mb_strlen($params['POST']['projectname']) <= 12;
                $validParent = trim($params['POST']['parent']);
                $validIcon = intval($params['POST']['projecticon']) > 0 && intval($params['POST']['projecticon'] <=125);

                $sql = "UPDATE `projects` SET ";

                if ($validProjectName){
                    $sql .= "`name`=:projectName ";
                }

                if ($flag) {
                    if ($validParent && $validProjectName) {
                        $sql .= ", `parentId`=:parentId ";
                    } elseif ($validParent) {
                        $sql .= "`parentId`=:parentId ";
                    } elseif (($params['POST']['parent'] == 0) && $validProjectName) {
                        $sql .= ", `parentId`=0 ";
                    } elseif ($params['POST']['parent'] == 0) {
                        $sql .= "`parentId`=0 ";
                    }
                }

                if ($validIcon && (($params['POST']['parent'] == 0) || $validParent || $validProjectName)){
                    $sql .= ", `iconId`=:iconId ";
                } elseif ($validIcon){
                    $sql .= "`iconId`=:iconId ";
                }

                $sql .= "WHERE `id` = :projectId AND `userId` = :userId";
                $db = new \TODO\main\Db();
                $res = $db->getInstance()->prepare($sql);

                if ($validProjectName){
                    $res->bindValue(':projectName', $params['POST']['projectname'], \PDO::PARAM_STR);
                }

                if ($validParent && $flag) {
                    $res->bindValue(':parentId', $params['POST']['parent'], \PDO::PARAM_INT);
                }

                if ($validIcon){
                    $res->bindValue(':iconId', $params['POST']['projecticon'], \PDO::PARAM_INT);
                }

                $res->bindValue(':projectId', $params[0], \PDO::PARAM_INT);
                $res->bindValue(':userId', $_SESSION['userId'], \PDO::PARAM_INT);
                $res->execute();
                return true;
            } else {
                $_SESSION['message_tasks'] = 'INVALID DATA';
                return false;
            }
        }
    }

    public function add($params) {

        if (empty($params['POST']['projectname']) || intval($params['POST']['parent']) < 0 || intval($params['POST']['projecticon']) < 0 || intval($params['POST']['projecticon']) > 125) {
            if (isset($params['POST']['flag'])) {
                $_SESSION['message_tasks'] = 'INVALID DATA';
            }
            return false;
        } else {

            $db = new \TODO\main\Db();
            $sql ='SELECT `id` FROM `projects` WHERE `userId` = :userId AND `id` = :parent AND `deleted` = 0 LIMIT 1';
            $prep = $db->getInstance()->prepare($sql);
            $prep->bindValue(':parent', $params['POST']['parent'], \PDO::PARAM_INT);
            $prep->bindValue(':userId', $_SESSION['userId'], \PDO::PARAM_INT);
            $prep->execute();
            $results = $prep->fetchAll();

            if ((!empty($results) && intval($params['POST']['parent']) > 0) || intval($params['POST']['parent']) == 0) {
                if (mb_strlen($params['POST']['projectname']) < 13) {
                    $sql = "INSERT INTO `projects`( `name`, `parentId`, `iconId`, `userId`) VALUES (:projectname, :parent, :projecticon, :userId)";
                    $db = new \TODO\main\Db();
                    $res = $db->getInstance()->prepare($sql);
                    $res->bindValue(':projectname', $params['POST']['projectname'], \PDO::PARAM_STR);
                    $res->bindValue(':parent', $params['POST']['parent'], \PDO::PARAM_INT);
                    $res->bindValue(':projecticon', $params['POST']['projecticon'], \PDO::PARAM_INT);
                    $res->bindValue(':userId', $_SESSION['userId'], \PDO::PARAM_INT);
                    $res->execute();
                    return true;
                } else {
                    $_SESSION['message_tasks'] = 'Name is to long';
                    return false;
                }
            } else {
                $_SESSION['message_tasks'] = 'INVALID DATA';
                return false;
            }
        }
    }

    public function prepare($params) {

        $all = $this->all();
        $all = array_combine(range(1, count($all)), array_values($all));

        $id = [];
        array_push($id, intval($params));

        foreach ($all as $project) {
            if ($project['parentId'] == intval($params)){
                $_SESSION['message_tasks'] = 'Project which you are trying to delete has subprojects. If you delete this project all subprojects also will be deleted with all included tasks. Are you sure?';
                return false;
            }
        }

        $db = new \TODO\main\Db();
        $sql ='SELECT `id` FROM `tasks` WHERE `userId` = :userId AND `projectId` = :projectId AND `deleted` = 0 LIMIT 1';
        $prep = $db->getInstance()->prepare($sql);
        $prep->bindValue(':userId', $_SESSION['userId'], \PDO::PARAM_INT);
        $prep->bindValue(':projectId', intval($params), \PDO::PARAM_INT);
        $prep->execute();

        $results = $prep->fetch();

        if (!empty($results)){
            $_SESSION['message_tasks'] = 'Project which you are trying to delete has tasks. If you delete this project all tasks also will be deleted. Are you sure?';
            return false;
        } else {
            return true;
        }
    }

    public function delete($params) {

        if ($params === '0') {
            $_SESSION['message_tasks'] = 'INVALID DATA';
            return false;
        }

        $all = $this->all();
        $all = array_combine(range(1, count($all)), array_values($all));

        $id = [];
        array_push($id, intval($params));

        foreach ($all as $project) {
            if ($project['parentId'] == intval($params)){
                array_push($id, $project['id']);
                foreach ($all as $project2) {
                    if ($project['id'] == $project2['parentId']){
                        array_push($id, $project2['id']);
                    }
                }
            }
        }

        $db = new \TODO\main\Db();
        $sql ='UPDATE `projects` SET `deleted` = 1 WHERE `userId` = :userId AND `id` in ( ';

        for ($i = 0; $i < count($id); $i++) {
            $sql .= ":id$i, ";
        }
        $sql = rtrim($sql, ', ');
        $sql .= ');';

        $prep = $db->getInstance()->prepare($sql);
        for ($i = 0; $i < count($id); $i++) {
            $prep->bindValue(":id$i", $id[$i], \PDO::PARAM_INT);
        }
        $prep->bindValue(':userId', $_SESSION['userId'], \PDO::PARAM_INT);
        $prep->execute();

        $sql ='UPDATE `tasks` SET `deleted` = 1 WHERE `userId` = :userId AND `projectId` in ( ';

        for ($i = 0; $i < count($id); $i++) {
            $sql .= ":id$i, ";
        }
        $sql = rtrim($sql, ', ');
        $sql .= ');';

        $prep = $db->getInstance()->prepare($sql);
        for ($i = 0; $i < count($id); $i++) {
            $prep->bindValue(":id$i", $id[$i], \PDO::PARAM_INT);
        }
        $prep->bindValue(':userId', $_SESSION['userId'], \PDO::PARAM_INT);
        $prep->execute();
        return true;
    }
}