<?php namespace TODO\models;

class Tasks {

    public function all() {
        $db = new \TODO\main\Db();
        $sql ='SELECT `tasks`.`id`, `tasks`.`name`, DATE_FORMAT(`tasks`.`dueDate`, "%Y-%m-%d %H:%i") AS "dueDate", `tasks`.`priority`, `tasks`.`done`, `tasks`.`projectId` FROM `tasks` INNER JOIN `projects` ON `tasks`.`projectId` = `projects`.`id` WHERE `tasks`.`userId` = :id AND `tasks`.`deleted` = 0 ORDER BY `dueDate` ASC';
        $prep = $db->getInstance()->prepare($sql);
        $prep->bindValue(':id', $_SESSION['userId'], \PDO::PARAM_INT);
        $prep->execute();

        $_SESSION['sort']['method'] = 1;
        $_SESSION['sort']['id'] = 1;
        return $results = $prep->fetchAll();
    }

    public function one($params) {
        $db = new \TODO\main\Db();
        $sql ='SELECT `id`, `name`, DATE_FORMAT(`dueDate`, "%Y-%m-%d %H:%i") AS "dueDate", `priority`, `done`, `projectId` FROM `tasks` WHERE `userId` = :userId AND `id` = :taskId AND `deleted` = 0 LIMIT 1';
        $prep = $db->getInstance()->prepare($sql);
        $prep->bindValue(':userId', $_SESSION['userId'], \PDO::PARAM_INT);
        $prep->bindValue(':taskId', intval($params), \PDO::PARAM_INT);
        $prep->execute();

        $results = $prep->fetch();

        if (!empty($results)){
            return $results;
        } else {
            $_SESSION['message_tasks'] = 'There is no such task or You do not have permission to see this task.';
            return false;
        }
    }

    public function byProject($params) {
        $db = new \TODO\main\Db();
        $sql ='SELECT `id`, `name`, DATE_FORMAT(`dueDate`, "%Y-%m-%d %H:%i") AS "dueDate", `priority`, `done`, `projectId` FROM `tasks` WHERE `userId` = :id AND `projectId` = :projectId AND `deleted` = 0 ORDER BY `dueDate` ASC';
        $prep = $db->getInstance()->prepare($sql);
        $prep->bindValue(':id', $_SESSION['userId'], \PDO::PARAM_INT);
        $prep->bindValue(':projectId', intval($params), \PDO::PARAM_INT);
        $prep->execute();
        $results = $prep->fetchAll();

        $_SESSION['sort']['method'] = 2;
        $_SESSION['sort']['id'] = $params;
        if (!empty($results)) {
            return $results;
        } else {
            $_SESSION['message_tasks'] = 'Your project is empty or You do not have permission to see this project.';
            return false;
        }
    }

    public function byDate($params) {
        $date = intval($params);
        date_default_timezone_set('UTC');
        $nowDate = date("Y-m-d H:i:s");

        $sql = 'SELECT `tasks`.`id` AS `id`, `tasks`.`name` AS `name`, DATE_FORMAT(`tasks`.`dueDate`, "%Y-%m-%d %H:%i") AS "dueDate", `tasks`.`priority` AS `priority`, `tasks`.`done` AS `done`, `tasks`.`projectId` AS `projectId` FROM `tasks` INNER JOIN `projects` ON (`tasks`.`userId` = `projects`.`userId` AND `tasks`.`projectId` = `projects`.`id` ) WHERE `tasks`.`userId` = :id AND `tasks`.`deleted` = 0';

        switch ($date){
            default:
            case '1': // ALL
                 $sql .= ' ORDER BY `dueDate` ASC';
                break;

            case '2': // OVERDUE
                $sql .= ' AND DATE_FORMAT(`dueDate`, "%Y-%m-%d %H:%i") < DATE_FORMAT("'.$nowDate.'", "%Y-%m-%d %H:%i") ORDER BY `dueDate` ASC';
                break;

            case '3': // TODAY
                $sql .= ' AND DATE_FORMAT(`dueDate`, "%Y-%m-%d") = DATE_FORMAT("'.$nowDate.'", "%Y-%m-%d") ORDER BY `dueDate` ASC';
                break;

            case '4': // WEEK
                $endDate = date("Y-m-d", strtotime("+7 days"));
                $sql .= ' AND DATE_FORMAT(`dueDate`, "%Y-%m-%d") > DATE_FORMAT("'.$nowDate.'", "%Y-%m-%d") AND DATE_FORMAT(`dueDate`, "%Y-%m-%d") < DATE_FORMAT("'.$endDate.'", "%Y-%m-%d") ORDER BY `dueDate` ASC';
                break;

            case '5': // MONTH
                $endDate = date("Y-m-d", strtotime("+1 month"));
                $sql .= ' AND DATE_FORMAT(`dueDate`, "%Y-%m-%d") > DATE_FORMAT("'.$nowDate.'", "%Y-%m-%d") AND DATE_FORMAT(`dueDate`, "%Y-%m-%d") < DATE_FORMAT("'.$endDate.'", "%Y-%m-%d") ORDER BY `dueDate` ASC';
                break;
        };

        $db = new \TODO\main\Db();
        $prep = $db->getInstance()->prepare($sql);
        $prep->bindValue(':id', $_SESSION['userId'], \PDO::PARAM_INT);
        $prep->execute();
        $results = $prep->fetchAll();

        $_SESSION['sort']['method'] = 1;
        $_SESSION['sort']['id'] = $params;
        if (!empty($results)) {
            return $results;
        } else {
            $_SESSION['message_tasks'] = 'There is no tasks';
            return false;
        }
    }

    private function validateDate($date, $format = 'Y-m-d H:i') {
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    public function done($params) {
        $db = new \TODO\main\Db();
        $sql ='UPDATE `tasks` SET `done` = 1 WHERE `tasks`.`id` = :id AND `userId` = :userId;';
        $prep = $db->getInstance()->prepare($sql);
        $prep->bindValue(':id', $params, \PDO::PARAM_INT);
        $prep->bindValue(':userId', $_SESSION['userId'], \PDO::PARAM_INT);
        $prep->execute();
    }

    public function undone($params) {
        $db = new \TODO\main\Db();
        $sql ='UPDATE `tasks` SET `done` = 0 WHERE `tasks`.`id` = :id AND `userId` = :userId;';
        $prep = $db->getInstance()->prepare($sql);
        $prep->bindValue(':id', $params, \PDO::PARAM_INT);
        $prep->bindValue(':userId', $_SESSION['userId'], \PDO::PARAM_INT);
        $prep->execute();
    }

    public function delete($params) {
        $db = new \TODO\main\Db();
        $sql ='UPDATE `tasks` SET `deleted` = 1 WHERE `id` = :id AND `userId` = :userId;';
        $prep = $db->getInstance()->prepare($sql);
        $prep->bindValue(':id', $params, \PDO::PARAM_INT);
        $prep->bindValue(':userId', $_SESSION['userId'], \PDO::PARAM_INT);
        $prep->execute();
    }

    public function prepare($params) {

        $db = new \TODO\main\Db();
        $sql ='SELECT `id` FROM `tasks` WHERE `userId` = :userId AND `id` = :taskId AND `deleted` = 0 LIMIT 1';
        $prep = $db->getInstance()->prepare($sql);
        $prep->bindValue(':userId', $_SESSION['userId'], \PDO::PARAM_INT);
        $prep->bindValue(':taskId', intval($params), \PDO::PARAM_INT);
        $prep->execute();

        $results = $prep->fetch();

        if (empty($results)){
            $_SESSION['message_tasks'] = 'INVALID DATA';
            return false;
        } else {
            $_SESSION['message_tasks'] = 'You can not undo deleting task. Are you sure?';
            return true;
        }
    }

    public function add($params) {
        if (empty($params['POST']['taskname']) || empty($params['POST']['taskpriority']) || empty($params['POST']['taskproject']) || empty($params['POST']['taskdate'])) {
            if (isset($params['POST']['flag'])) {
                $_SESSION['message_tasks'] = 'INVALID DATA';
            }
            return false;
        } else {

            $db = new \TODO\main\Db();
            $sql ='SELECT `id` FROM `projects` WHERE `userId` = :userId AND `id` = :taskproject LIMIT 1';
            $prep = $db->getInstance()->prepare($sql);
            $prep->bindValue(':taskproject', $params['POST']['taskproject'], \PDO::PARAM_INT);
            $prep->bindValue(':userId', $_SESSION['userId'], \PDO::PARAM_INT);
            $prep->execute();
            $results = $prep->fetchAll();

            if ($results && (trim($params['POST']['taskname']) !== '') && ((trim($params['POST']['taskpriority']) !== '') && ($params['POST']['taskpriority'] > 0) && ($params['POST']['taskpriority'] < 4)) && (trim($params['POST']['taskproject']) !== '') && ($this->validateDate($params['POST']['taskdate'])) ) {
                $datetime = new \DateTime($params['POST']['taskdate']);
                $taskdate = $datetime->format('Y-m-d H:i');
                $sql = "INSERT INTO `tasks`(`userId`, `name`, `dueDate`, `priority`, `projectId`) VALUES (:userId,:taskname,'$taskdate',:taskpriority,:taskproject)";

                $db = new \TODO\main\Db();
                $res = $db->getInstance()->prepare($sql);
                $res->bindValue(':taskname', $params['POST']['taskname'], \PDO::PARAM_STR);
                $res->bindValue(':taskpriority', $params['POST']['taskpriority'], \PDO::PARAM_INT);
                $res->bindValue(':taskproject', $params['POST']['taskproject'], \PDO::PARAM_INT);
                $res->bindValue(':userId', $_SESSION['userId'], \PDO::PARAM_INT);
                $res->execute();
                return true;
            } else {
                $_SESSION['message_tasks'] = 'INVALID DATA';
                return false;
            }
        }
    }

    public function edit($params) {
        if (empty($params['POST'])) {
            $_SESSION['message_tasks'] = 'INVALID DATA';
            return false;
        } else {

            $db = new \TODO\main\Db();
            $sql ='SELECT `id`FROM `tasks` WHERE `userId` = :userId AND `id` = :taskId LIMIT 1';
            $prep = $db->getInstance()->prepare($sql);
            $prep->bindValue(':taskId', $params[0], \PDO::PARAM_INT);
            $prep->bindValue(':userId', $_SESSION['userId'], \PDO::PARAM_INT);
            $prep->execute();
            $results = $prep->fetchAll();

            $db = new \TODO\main\Db();
            $sql ='SELECT `id` FROM `projects` WHERE `userId` = :userId AND `id` = :taskProject LIMIT 1';
            $prep = $db->getInstance()->prepare($sql);
            $prep->bindValue(':taskProject', $params['POST']['taskproject'], \PDO::PARAM_INT);
            $prep->bindValue(':userId', $_SESSION['userId'], \PDO::PARAM_INT);
            $prep->execute();
            $results2 = $prep->fetchAll();

            if ($results && $results2) {

                $validName = trim($params['POST']['taskname']) !== '';
                $validDate = (trim($params['POST']['taskdate']) !== '') && ($this->validateDate($params['POST']['taskdate']));
                $validPriority = ((trim($params['POST']['taskpriority']) !== '') && ($params['POST']['taskpriority'] > 0) && ($params['POST']['taskpriority'] < 4));
                $validProject = trim($params['POST']['taskproject']) !== '';

                $sql = "UPDATE `tasks` SET ";

                if ($validName){
                    $sql .= "`name`=:taskName ";
                }

                if (($validDate) && ($validName)){
                    $sql .= ", `dueDate`=:taskDate ";
                } else if ($validDate) {
                    $sql .= "`dueDate`=:taskDate ";
                }

                if ($validPriority && ($validName || $validDate)){
                    $sql .= ", `priority`=:taskPriority ";
                } else if ($validPriority) {
                    $sql .= "`priority`=:taskPriority ";
                }

                if ($validProject && ($validName || $validDate || $validPriority)){
                    $sql .= ", `projectId`=:taskProject ";
                } else if ($validProject) {
                    $sql .= "`projectId`=:taskProject ";
                }

                $sql .= "WHERE `id` = :id AND `userId` = :userId";
                $datetime = new \DateTime($params['POST']['taskdate']);
                $taskdate = $datetime->format('Y-m-d H:i');

                $db = new \TODO\main\Db();
                $res = $db->getInstance()->prepare($sql);

                if ($validName){
                    $res->bindValue(':taskName', $params['POST']['taskname'], \PDO::PARAM_STR);
                }

                if ($validDate) {
                    $res->bindValue(':taskDate', $taskdate, \PDO::PARAM_STR);
                }

                if ($validPriority) {
                    $res->bindValue(':taskPriority', $params['POST']['taskpriority'], \PDO::PARAM_INT);
                }

                if ($validProject) {
                    $res->bindValue(':taskProject', $params['POST']['taskproject'], \PDO::PARAM_INT);
                }

                $res->bindValue(':id', $params[0], \PDO::PARAM_INT);
                $res->bindValue(':userId', $_SESSION['userId'], \PDO::PARAM_INT);
                $res->execute();
                return true;
            } else {
                $_SESSION['message_tasks'] = 'INVALID DATA';
                return false;
            }
        }
    }
}