<?php namespace TODO\models;

class Menus {

    public function all() {
        $db = new \TODO\main\Db();
        $sql ='SELECT `id`, `name`, `menuId` FROM `menus` WHERE 1';
        $prep = $db->getInstance()->prepare($sql);
        $prep->execute();
        return $results = $prep->fetchAll();
    }
}