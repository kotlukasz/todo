<?php namespace TODO\models;

class Icons {

    public function all() {
        $db = new \TODO\main\Db();
        $sql ='SELECT `id`, `name`, `unicode` FROM `icons` WHERE 1';
        $prep = $db->getInstance()->prepare($sql);
        $prep->execute();
        return $results = $prep->fetchAll();
    }
}