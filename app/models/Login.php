<?php namespace TODO\models;

class Login {

    private $options = array('cost' => 12);

    public function Login($params=[]){

        if (empty($params['POST'])) {
            return false;
        } else {
            $db = new \TODO\main\Db();
            $sql ='SELECT `id`, `password` FROM `users` WHERE `mail` = :mail LIMIT 1';
            $prep = $db->getInstance()->prepare($sql);
            $prep->bindValue(':mail', $params['POST']['email'], \PDO::PARAM_STR);
            $prep->execute();
            $results = $prep->fetchAll();

            if (password_verify($params['POST']['password'], $results[0]['password'])) {
                if (password_needs_rehash($results[0]['password'], PASSWORD_DEFAULT, $this->options)) {
                    $newHash = password_hash($params['POST']['password'], PASSWORD_DEFAULT, $this->options);
                    $sql ='UPDATE `users` SET `password`=:password WHERE `id` = :id AND `mail` = :mail';
                    $prep = $db->getInstance()->prepare($sql);
                    $prep->bindValue(':id', $results[0]['id'], \PDO::PARAM_INT);
                    $prep->bindValue(':mail', strtolower($params['POST']['email']), \PDO::PARAM_STR);
                    $prep->bindValue(':password', $newHash, \PDO::PARAM_STR);
                    $prep->execute();
                }
                $_SESSION['userId'] = $results[0]['id'];
            } else {
                $_SESSION['message_login'] = 'Wrong Email and password combination.';
                return false;
            }
            return false;
        }
    }

    public function Registry($params=[]){

        if (($params['POST']['email'] == $params['POST']['repeat-email']) && ($params['POST']['password'] == $params['POST']['repeat-password'])) {

            if (filter_var($params['POST']['email'], FILTER_VALIDATE_EMAIL)) {

                $newHash = password_hash($params['POST']['password'], PASSWORD_DEFAULT, $this->options);

                $db = new \TODO\main\Db();
                $sql = 'SELECT `id` FROM `users` WHERE `mail` = :mail LIMIT 1';
                $prep = $db->getInstance()->prepare($sql);
                $prep->bindValue(':mail', $params['POST']['email'], \PDO::PARAM_STR);
                $prep->execute();
                $results = $prep->fetchAll();

                if (empty($results)) {

                    if (strlen($params['POST']['password']) > 7) {

                        $sql = 'INSERT INTO `users`(`mail`, `password`) VALUES (:mail,:password)';
                        $prep = $db->getInstance()->prepare($sql);
                        $prep->bindValue(':mail', strtolower($params['POST']['email']), \PDO::PARAM_STR);
                        $prep->bindValue(':password', $newHash, \PDO::PARAM_STR);
                        $prep->execute();
                    } else {
                        $_SESSION['message_registry'] = 'Your password is too short. Minimum 8 characters.';
                        return false;
                    }
                } else {
                    $_SESSION['message_registry'] = 'Entered E-mail is used try to login.';
                    return false;
                }
                $_SESSION['message_registry'] = 'Successful registration';
                return true;
            } else {
                $_SESSION['message_registry'] = 'Invalid e-mail';
                return false;
            }
        } else {
            $_SESSION['message_registry'] = 'Invalid data';
            return false;
        }
    }
}