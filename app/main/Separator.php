<?php namespace TODO\main;

/**
 * Class Separator
 * @package TODO\main
 *
 * Cut url in parts to give name of controller, method in controller and params for method
 *
 */

class Separator {

    private $controller;
    private $action;
    private $params = [];

    public function __construct() {
        if (isset($_GET['url'])) {
            $path = explode('/', $_GET['url']);
            if (isset($path[0])) $this->controller = $path[0];
            if (isset($path[1])) $this->action = $path[1];
            if (isset($path[2])) {
                array_shift($path);
                array_shift($path);
                $this->params = $path;
            }
        }
        if (!empty($_POST)) {
            $this->params['POST'] = $_POST;
        }
    }

    /**
     * @return string
     */
    public function getController() {
        return ucfirst(strtolower($this->controller));
    }

    /**
     * @param string $controller
     */
    public function setController($controller) {
        $this->controller = $controller;
    }

    /**
     * @return string
     */
    public function getAction() {
        return strtolower($this->action);
    }

    /**
     * @param string $action
     */
    public function setAction($action) {
        $this->action = $action;
    }

    /**
     * @return array
     */
    public function getParams() {
        return $this->params;
    }

    /**
     * @param array $params
     */
    public function setParams($params) {
        $this->params = $params;
    }
}