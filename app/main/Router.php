<?php namespace TODO\main;

/**
 * Class Router
 * @package TODO\main
 *
 * Use information from Separator to run correct piece of code. Check if exists if not use info from config file.
 *
 */

class Router {

    private $controller;
    private $action;
    private $params;

    public function __construct($path) {

        $config = new \TODO\main\Config();
        $config = $config->setConfig(['paths','defaults']);

        if ($path instanceof \TODO\main\Separator) {
            $this->controller = $path->getController();
            $this->action = $path->getAction();
            $this->params = $path->getParams();

            $url = $config['controllers_path'].$this->controller.'.php';

            if (file_exists($url)) {
                require_once $url;
                $temp = '\TODO\controllers\\'.$this->controller;
                $call = new $temp();
            } else {
                $this->controller = $config['default_controller'];
                require_once $config['controllers_path'].$this->controller.'.php';
                $temp = '\TODO\controllers\\'.$this->controller;
                $call = new $temp();
            }

            $temp = '\TODO\controllers\\'.$this->controller;
            if (method_exists($temp, $this->action)){
                $call->{$this->action}($this->params);
            } else {
                $this->action = $config['default_method'];
                $call->{$this->action}($this->params);
            }
        }
    }
}