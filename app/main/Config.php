<?php namespace TODO\main;

class Config {

    private $configs = [];
    private $database = [];
    private $paths = [];
    private $defaults = [];

    private function buildConfig() {

        $DS = '/';

        $baseURL = 'http://';

        if ( isset($_SERVER['HTTPS']) ) {
            if ( 'on' == strtolower($_SERVER['HTTPS']) )
                $baseURL = 'https://';
            if ( '1' == $_SERVER['HTTPS'] )
                $baseURL = 'https://';
        } elseif ( isset($_SERVER['SERVER_PORT']) && ( '443' == $_SERVER['SERVER_PORT'] ) ) {
            $baseURL = 'https://';
        } elseif ( isset($_SERVER['HTTP_X_FORWARDED_PORT']) && $_SERVER['HTTP_X_FORWARDED_PORT'] == '443') {
            $baseURL = 'https://';
        }

        //$baseURL = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://';
        $baseURL .= $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']);
        $slash = substr($baseURL, -1);
        $baseURL = $slash != '/' ? $baseURL.'/' : $baseURL;

        $app_dir = 'app';
        $controllers_dir = 'controllers';
        //$main_dir = 'main';
        $media_dir = 'media';
        $css_dir = 'css';
        $img_dir = 'img';
        $js_dir = 'js';
        $models_dir = 'models';
        $views_dir = 'views';
        //$vendor_dir = 'vendor';

        $this->database = [
            'host' => 'sql.luxter.home.pl',
            'name' => '05690700_todo',
            'user' => '05690700_todo',
            'pass' => 'L&L,T+rhBP.b'
        ];

        $this->defaults = [
            'baseURL' => $baseURL,
            'default_controller' => 'Tasks',
            'default_method' => 'home'
        ];

        $this->paths = [
            'app_path' => $app_dir.$DS,
            'controllers_path' => $app_dir.$DS.$controllers_dir.$DS,
            //'main_path' => $app_dir.$DS.$main_dir.$DS,
            'media_path' => $app_dir.$DS.$media_dir.$DS,
            'css_path' => $app_dir.$DS.$media_dir.$DS.$css_dir.$DS,
            'img_path' => $app_dir.$DS.$media_dir.$DS.$img_dir.$DS,
            'js_path' => $app_dir.$DS.$media_dir.$DS.$js_dir.$DS,
            'models_path' => $app_dir.$DS.$models_dir.$DS,
            'views_path' => $app_dir.$DS.$views_dir.$DS,
            //'vendor_path' => $app_dir.$DS.$vendor_dir.$DS
        ];
    }

    public function setConfig($name=[]) {
        $this->buildConfig();
        if (is_array($name)) {
            foreach ($name as $value) {
                if (isset($this->$value)) {
                    $this->configs = array_merge($this->configs, $this->$value);
                }
            }
        }
        return $this->configs;
    }
}