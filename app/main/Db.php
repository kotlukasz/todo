<?php namespace TODO\main;

/**
 * Class Db
 * @package TODO\main
 *
 * connect to DB based on data from config file
 *
 */

class Db {

    private $instance = NULL;

    public function getInstance() {
        if (!isset($this->instance)) {
            $pdoOptions = array(
                \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
                \PDO::ATTR_EMULATE_PREPARES => false,
                \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION);

            $config = new Config();
            $config = $config->setConfig(['database']);
            try {
                $this->instance = new \PDO('mysql:host='.$config['host'].';dbname='.$config['name'].';charset=utf8', $config['user'], $config['pass'], $pdoOptions);
            } catch (\PDOException $error){
                exit('Database error : ['.$error->getMessage().']');
            }
        }
        return $this->instance;
    }
}