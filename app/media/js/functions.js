function showEditProjects() {
    $(".project > a").each(function () {
        var oldHref = $(this).attr("href");
        var newHref = $(this).attr("edithref");
        $(this).attr('href', newHref);
        $(this).attr('edithref', oldHref);
    });
    $(".editIcon").removeClass('fa-trash');
    $(".editIcon").toggleClass('fa-pencil');
}

function deleteProject() {
    $(".project > a").each(function () {
        var oldHref = $(this).attr("href");
        var newHref = $(this).attr("deletehref");
        $(this).attr('href', newHref);
        $(this).attr('deletehref', oldHref);
    });
    $(".editIcon").removeClass('fa-pencil');
    $(".editIcon").toggleClass('fa-trash');
}