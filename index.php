<?php
if (!isset($_SESSION)) session_start();

/**
 * ALL SESSION var to prevent errors
 */

if (!isset($_SESSION['userId'])) $_SESSION['userId'] = NULL;                    // user ID
if (!isset($_SESSION['oldUrl'])) $_SESSION['oldUrl'] = '';                      // URL if not login to return after login
if (!isset($_SESSION['message_login'])) $_SESSION['message_login'] = '';        // error message while login incorrect
if (!isset($_SESSION['message_registry'])) $_SESSION['message_registry'] = '';  // error message while registry incorrect
if (!isset($_SESSION['message_tasks'])) $_SESSION['message_tasks'] = '';        // error message while no tasks to show or wrong project id
if (!isset($_SESSION['sort']['method'])) $_SESSION['sort']['method'] = NULL;    // 1 - byDate, 2 - byProject
if (!isset($_SESSION['sort']['id'])) $_SESSION['sort']['id'] = NULL;            // ID of element from chosen method

/**
 * COMPOSER'S autoloader
 */

require_once 'vendor/autoload.php';

/**
 * If not log in remember all given url except links start with 'login'
 */

if ($_SESSION['userId'] === NULL) {
    if (mb_substr(strtolower($_GET['url']), 0, 5) !== 'login') {
        $_SESSION['oldUrl'] = $_GET['url'];
        $_GET['url'] = 'login';
    }
}

/**
 * Separator cut url in parts to give name of controller, method in controller and params for method
 * Router use this information to run correct piece of code
 */

$path = new \TODO\main\Separator();
$router = new \TODO\main\Router($path);